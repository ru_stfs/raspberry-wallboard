#!/bin/bash

export DISPLAY=:0

if [ -z "$1" ] ; then
  >&2 echo "No arguments specified. Usage: $0 on|off"
  exit -1
fi

if [ "$1" == "on" ] ; then
  echo "$(date) -- turning monitor on"
  tvservice --preferred
  CURR_VT=$(sudo fgconsole)
  NEXT_VT=$((CURR_VT+1))
  sudo chvt ${NEXT_VT} && sudo chvt ${CURR_VT}
  xset dpms force on
  xset s off
  xset -dpms
  xset s noblank
elif [ "$1" == "off" ] ; then
  echo "$(date) -- turning monitor off"
  tvservice --off
else
  >&2 echo "Invalid argument '$1'. Usage: $0 on|off"
fi
