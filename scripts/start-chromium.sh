#!/bin/bash

export DISPLAY=:0

chromium-browser --start-maximized --start-fullscreen --restore-last-session --disable-infobars --disable-session-crashed-bubble --disable-translate
